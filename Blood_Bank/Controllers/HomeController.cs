﻿using Blood_Bank.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Blood_Bank.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}