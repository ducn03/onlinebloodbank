﻿using Microsoft.AspNetCore.Mvc;

namespace Blood_Bank.Controllers
{
    public class UserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult DonorRegistration()
        {
            return View();
        }
        public IActionResult TermsOfService()
        {
            return View();
        }
    }
}
